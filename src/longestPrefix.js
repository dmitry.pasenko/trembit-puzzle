function getPrefix(str1, str2) {
    const n = Math.min(str1.length, str2.length);
    let prefix = '';

    for (let i = 0; i < n; i++) {
        if (str1[i] === str2[i]) {
            prefix += str1[i];
        } else {
            break;
        }
    }

    return prefix;
}

function longestPrefix(arr) {
    let result = '';
    for (let i = 0; i < arr.length; i++) {
        for (let j = i + 1; j < arr.length; j++) {
            const prefix = getPrefix(arr[i], arr[j]);
            if (prefix.length > result.length) {
                result = prefix;
            }
        }
    }
    return result;
}

module.exports = { getPrefix, longestPrefix };

if (require.main === module) {
    const args = process.argv.slice(2);
    if (args.length === 0) {
        console.error('Please provide an array of strings as arguments.');
        process.exit(1);
    }
    console.log(longestPrefix(args));
}
