# Trembit puzzle

This project contains functions to find the longest common prefix in an array of strings and tests for these functions.

## Puzzle description
Given array of strings. Need to find the longest common prefix for at least 2 elements for that array.
Note:
* it is enough prefix to be common for 2 elements. 
* No need to have it in all array elements;
* prefix is the starting sequence of chars in a string.
* if there are several common prefixes, then the longest must be returned
* pls don’t use regex

## Installation

Run `npm install` to install the dependencies.

## Running Tests

Run `npm test` to execute the tests using Jest.

## Running the Program

Run `npm run run -- <array of strings>` to execute the program and find the longest common prefix among the provided strings.

Example:

```sh
npm run run -- a aa aaa foo foot xxxxx
