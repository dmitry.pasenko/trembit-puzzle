const { getPrefix, longestPrefix } = require('../src/longestPrefix');

test('getPrefix correctly finds common prefix of two strings', () => {
    expect(getPrefix('hello', 'helium')).toBe('hel');
    expect(getPrefix('foo', 'foobar')).toBe('foo');
    expect(getPrefix('abc', 'def')).toBe('');
    expect(getPrefix('same', 'same')).toBe('same');
});

test('longestPrefix correctly finds longest common prefix among array elements', () => {
    expect(longestPrefix(['a', 'aa', 'aaa', 'foo', 'foot', 'xxxxx'])).toBe('foo');
    expect(longestPrefix(['basdf', 'badfdfs', 'baddsfs', 'bassdad'])).toBe('bas');
    expect(longestPrefix(['cat', 'car', 'dog'])).toBe('ca');
    expect(longestPrefix(['apple', 'ape', 'apasdsad'])).toBe('ap');
    expect(longestPrefix(['apple', 'banana', 'cherry'])).toBe('');
    expect(longestPrefix(['prefix', 'prelude', 'prevent'])).toBe('pre');
    expect(longestPrefix(['one', 'two', 'three'])).toBe('t');
    expect(longestPrefix(['abc', 'abcdef', 'ab', 'abcd'])).toBe('abcd');
    expect(longestPrefix(['same', 'same', 'same'])).toBe('same');
    expect(longestPrefix(['no', 'common', 'prefix'])).toBe('');
});
